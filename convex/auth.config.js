export default {
  providers: [
    {
      domain: 'https://amused-garfish-83.clerk.accounts.dev',
      applicationID: 'convex'
    }
  ]
}